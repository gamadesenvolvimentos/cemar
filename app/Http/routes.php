<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/clinica-urologia-cemar-barretos', [
    'as' => 'perfil-urologia', 'uses' => 'SiteController@getUrologia'
]);

Route::get('/clinica-dermatologia-cemar-barretos', [
    'as' => 'perfil-dermatologia', 'uses' => 'SiteController@getDermatologia'
]);

Route::get('/clinica-reumatologia-cemar-barretos', [
    'as' => 'perfil-reumatologia', 'uses' => 'SiteController@getReumatologia'
]);

Route::get('/clinica-otorrinolaringologia-cemar-barretos', [
    'as' => 'perfil-otorrino', 'uses' => 'SiteController@getOtorrino'
]);

Route::get('/clinica-endocrinologia-cemar-barretos', [
    'as' => 'perfil-endocrinologia', 'uses' => 'SiteController@getEndocrino'
]);

Route::get('/clinica-neurologia-cemar-barretos', [
    'as' => 'perfil-neurologia', 'uses' => 'SiteController@getNeurologia'
]);

/*Noticias Urologia*/
Route::get('/urologia-calculo-renal-mitos-e-vedades', [
    'as' => 'mito-caulculo-renal', 'uses' => 'SiteController@getMitosCalculo'
]);

Route::get('/urologia-estudo-urodinamico', [
    'as' => 'mito-caulculo-renal', 'uses' => 'SiteController@getEstudoUrodinamico'
]);
/*Noticias Urologia*/

//Corpo Clínico routes

Route::get('/corpoclinico/{$medico}', [
    'as' => 'corpoclinico', 'uses' => 'SiteController@getCorpoclinico'
]);


Route::controller('/', 'SiteController');
