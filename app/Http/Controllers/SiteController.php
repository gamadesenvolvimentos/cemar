<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

$GLOBALS['title'] =  "CEMAR - Centro Médico Alessi e Rocha";

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {   
        
        $data = [

            'menu' => [ 'active' => 'home' ]

       ];
       
       return view('home', $data);
    }

public function getAclinica()
    {   

        $GLOBALS['title'] =  "CEMAR - Conheça a Clínica";
        $data = [

            'menu' => [ 'active' => 'aclinica' ]

       ];
       
       return view('aclinica', $data);
    }

public function getCorpoclinico($medico=null)
    {   
        $GLOBALS['title'] =  "CEMAR - Corpo Clínico -";

        if($medico != null) {
           $GLOBALS['title'] .=  " ".$medico; 
        } 

        $data = [

            'menu' => [ 'active' => 'corpoclinico' ],
            'medico' => ['medico' => $medico]

       ];
       
       return view('corpoclinico', $data);
    }


    public function getSugestoes()
    {   
        $GLOBALS['title'] =  "CEMAR - Sugestões ou Reclamações";
        $data = [

            'menu' => [ 'active' => 'aclinica' ]

       ];
       
       return view('sugestoes', $data);
    }

    public function postSugestoes()
    {
      
      $data = \Input::all();


      \Mail::send("emails.sugestao", $data, function($message) use ($data){ 
        
        $message
          ->from("contato@gamadesenvolvimentos.com.br")
          ->to("drgustavorocha@yahoo.com.br", "Site Cemar.med.br")
          ->subject("[Sugestões e Reclamações] - ".$data["name"]);
      });

      \Session::flash('success', true);

      return \Redirect::back();
    
    }


    public function getConvenios()
    {   
        $GLOBALS['title'] =  "CEMAR - Convênios Atendidos";
        $data = [

            'menu' => [ 'active' => 'convenios' ]

       ];
       
       return view('convenios', $data);
    }

    public function getArtigos()
    {   
        $GLOBALS['title'] =  "CEMAR - Artigos - Leituras";
        $data = [

            'menu' => [ 'active' => 'artigos' ]

       ];
       
       return view('artigos', $data);
    }

    public function getContato()
    {   
        $GLOBALS['title'] =  "CEMAR - Entre em contato conosco";
        $data = [

            'menu' => [ 'active' => 'contato' ]

       ];
       
       return view('contato', $data);
    }

    public function postContato()
    {
      
      $data = \Input::all();


      \Mail::send("emails.form", $data, function($message) use ($data){ 
        
        $message
          ->from("contato@gamadesenvolvimentos.com.br")
          ->to("drgustavorocha@yahoo.com.br", "Site Cemar.med.br")
          ->subject("[Contato Site] - ".$data["name"]);
      });

      \Session::flash('success', true);

      return \Redirect::back();
    
    }


    public function getUrologia()
    {  
       $GLOBALS['title'] =  "CEMAR - Clínica Urologia Barretos";  
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('urologia', $data);
    }

    public function getDermatologia()
    {  

       $GLOBALS['title'] =  "CEMAR - Clínica Dermatologia Barretos";  
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('dermatologia', $data);
    }

    public function getReumatologia()
    {  
       $GLOBALS['title'] =  "CEMAR - Clínica Reumatologia Barretos";  
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('reumatologia', $data);
    }

    public function getOtorrino()
    {
       $GLOBALS['title'] =  "CEMAR - Clínica Otorrinolaringologia Barretos";
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('otorrinolaringologia', $data);
    }

    public function getEndocrino()
    {
       $GLOBALS['title'] =  "CEMAR - Clínica Endocrinologia Barretos";
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('endocrinologia', $data);
    }

    public function getNeurologia()
    {  
       $GLOBALS['title'] =  "CEMAR - Clínica Neurologia Barretos"; 
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('neurologia', $data);
    }

    public function getMitosCalculo()
    {  
       $GLOBALS['title'] =  "CEMAR - Urologia - Mitos e Verdades sobre cálculo renal"; 
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('mitoscalculo', $data);
    }

    public function getEstudoUrodinamico()
    {  
       $GLOBALS['title'] =  "CEMAR - Urologia - Estudo Urodinâmico"; 
       $data = [

            'menu' => [ 'active' => 'especialidades' ]

       ];
       
       return view('estudourodinamico', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
