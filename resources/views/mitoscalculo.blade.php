@include('partials.header')
    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
               Cálculo renal: mitos e verdades
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span><a href="/clinica-urologia-cemar-barretos">urologia</a> &nbsp;/&nbsp; cálculo renal: mitos e verdades </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        Só quem já passou por uma cólica renal sabe o tamanho da dor e sofrimento que isso pode causar. E não é difícil encontrar alguém que já passou por esse drama, afinal, a incidência de cálculo renal tem aumentado bastante. Estudos apontam que  cerca de 10% das pessoas terão uma ou mais crises em suas vidas. Em alguns casos os cálculos são eliminados sem maiores problemas, entretanto, é  importante ressaltar que, mesmos os casos assintomáticos ou silenciosos merecem tratamento, pois a chance de crescimento, migração e obstrução do rim chegam até 80% em alguns estudos. <br><br>
                        Apesar de acometer muitas pessoas, a doença ainda gera dúvidas e crenças entre a população Barretense, o que muitas vezes dificulta o diagnóstico e tratamento. Para diminuir a falta de informação sobre o tema, listamos alguns mitos e verdades que poderão ajudar você a compreender melhor o cálculo renal.
                    </p>
                    <p>    
                        <strong>Beber bastante água é o melhor “remédio” na prevenção do cálculo renal? </strong> <br>
                        VERDADE <br>
                        O cálculo nada mais é do que a união de cristais na urina que ao se juntarem nos rins formam uma massa maior, conhecida como “pedra nos rins”. Quanto mais água a pessoa tomar, menor a concentração destes cristais na urina e maior a chance de elimina-los. Por outro lado, com a baixa ingesta hídrica a urina fica concentrada o que favorece a agregação dos cristais que por conta disso precipitam na forma da litíase renal. Neste ponto vale salientar que outros fatores alimentares, genéticos e ambientais também contribuem para formação dos cálculos.
                    </p>
                    <p>
                        <strong>A água de Ibirá® é melhor do que as outras na prevenção do cálculo renal?</strong>
                        <br>MITO<br>
                        Não existe nenhum estudo científico  que demonstre superioridade de determinada marca sobre as outras na prevenção do cálculo renal. Deste modo todas as águas próprias para o consumo humano são adequadas no tratamento preventivo.
                    </p>
                    <p>
                        <strong>Beber 2 litros de água por dia é o mínimo necessário?</strong>
                        <br>VERDADE<br>
                        A necessidade de ingestão diária varia de acordo com o clima ( temperatura e umidade), atividade física, idade e peso da pessoa, mas geralmente o mínimo necessário são 2 litros por dia. Uma maneira prática de saber se está tomando a quantidade suficiente é observar  a coloração da urina, que deve sempre estar incolor ou amarelo claro. O ideal para pacientes formadores de cálculo renal é produzir cerca 2 litros de urina por dia.
                    </p>
                    <p>                        
                        <strong>“Frutas  ácidas” (laranja e limão) fazem mal aos rins?</strong>
                        <br>MITO<br>
                        Alimentos como laranja e limão, em forma de fruta ou suco, ajudam a dissolver o cálcio presente na urina, prevenindo o acúmulo e a formação de pedras no rim.
                    </p>
                    <p>
                        <strong>Todo paciente com cálculo renal sente dor?</strong>
                        <br>MITO<br>
                        Nem todos os cálculos renais ocasionam dor aos pacientes. Em alguns casos podem permanecer por anos no organismo sem serem notados. A dor está intimamente ligada a localização do cálculo e obstrução da urina.
                    </p>
                    <p>
                        <strong>Quanto maior o cálculo maior será a dor?</strong>
                        <br>MITO<br>
                        Não existe uma proporção direta entre o tamanho do cálculo e o tamanho da dor. Alguns cálculos bem pequenos podem desencadear dores inesquecíveis e raramente cálculos grandes serem eliminados sem maiores transtornos.
                    </p>
                    <p>
                        <strong>Quem já teve pedra nos rins tem mais chance de voltar a ter outras?</strong>
                        <br>VERDADE<br>
                        Sem tratamento preventivo a chance de recorrência para quem já teve pedra no rim é de 50% em 5 anos. Caso apresente mais de uma vez, a possibilidade de apresentar novos episódios sobe para 80%.
                    </p>     
                    <p>
                        <strong>O leite ajuda a formar cálculo renal?</strong>
                        <br>MITO<br>
                        Fala-se muito que o leite deve ser evitado porque tem cálcio na composição e pode provocar o surgimento de pedra nos rins. Na verdade, tirá-lo da dieta não resolve o problema e pode causar outra doença: a osteoporose (enfraquecimento dos ossos). Essa regra vale principalmente para as mulheres.
                    </p>
                    <P>
                        <strong>Cerveja quente ajuda a reduzir esse mal?</strong>
                        <br>MITO<br>
                        A cerveja não tem nenhum efeito na formação dos cálculos urinários. Além disso,  assim como todas as bebidas alcoólicas, se for tomada sem a devida hidratação, pode levar a desidratação depois de algumas horas, podendo aumentar a formação de cálculos. Em relação a ser tomada quente ou gelada, logo após chegar ao estômago, a cerveja já estará na mesma temperatura do corpo, independente de ser ingerida quente ou gelada.
                    </p>
                    <p>
                        <strong>Seus pais tiveram pedra nos rins que você obrigatoriamente terá?</strong>
                        <br>MITO<br>
                        Mas fique alerta: 10% dos casos ocorrem por causa de algum fator genético. Lembre-se de que os hábitos alimentares também passam de geração em geração. Então, nada como uma alimentação regulada para pôr fim a essa herança.
                    </p>
                    <p>
                        <strong>Consumir muita vitamina C pode causar pedra nos rins?</strong>
                        <br>VERDADE<br>
                        O risco aumenta nas dietas acima de 4 gramas ao dia de vitamina C. Por isso, quem tem propensão para formar pedras nos rins deve evitar automedicação com a vitamina
                    </p>
                    <p>    
                        <strong>O chá de quebra-pedra dissolve os cálculos?</strong>
                        <br>MITO<br>
                        Existem vários estudos sobre a ação do chá de quebra pedra (erva cientificamente chamada de Phyllanthus) nos cálculos renais e em nenhum ficou demonstrado que ele impeça a formação e muito menos que dissolva os cálculos já formados. Sendo assim, caso haja grande consumo desse chá, o efeito será semelhante ao aumento de ingesta de água.
                    </p>
                    <p>    
                        <strong>Frutas e verduras com sementes podem ocasionar cálculos?</strong>
                        <br>MITO<br>
                        O mito mais conhecido é que as sementes do tomate causam pedras nos rins. Não são as sementes que causam cálculos, mas sim determinadas substâncias contidas nos alimentos. 
                    </p>
                    <p>    
                        <strong>Dietas ricas em sódio ( sal ) colaboram com a formação de cálculos renais?</strong>
                        <br>VERDADE<br>
                        As dietas ricas em sódio levam a maior absorção do cálcio nos rins e a maior formação de pedras no rim.
                    </p>
                    <p>
                        <strong>Qualquer tipo de líquido é bom para evitar cálculo renal?</strong>
                        <br>MITO<br>
                        Os líquidos adequados para a prevenção da formação de cálculos renais são aqueles sem álcool, com muita água e pouco sódio em sua composição. Sendo assim refrigerante não vale.
                    </p>
                    <p>    
                        <strong>A atividade física em excesso, sem reposição adequada de líquidos, pode favorecer o aparecimento do cálculo renal? </strong>
                        <br>VERDADE<br>
                        Qualquer situação que reduza o volume urinário favorece a aproximação dos cristais e sua precipitação na urina. Por esse motivo, os atletas devem se hidratar adequadamente.
                    </p>

                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">

                        <h3 class="sidebox-title">Dr. Gustavo Rocha</h3>
                        <img src="/images/dr-gustavo.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="/corpoclinico">veja mais</a>   
                        </p>
                        <hr>
                            
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')