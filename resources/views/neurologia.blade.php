@include('partials.header')

    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Neurologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>neurologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        <img src="/images/neurologia.png">
                        Diagnosticar doenças do sistema nervoso central e periférico, tratar as diversas patologias neurológicas e prevenir complicações graves.
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        
                    </p>
                    <br>
                    <!-- <h3><i class="fa fa-question-circle"></i> Perguntas frequêntes sobre Neurologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> O que é Epilepsia? </h3>
                        <p class="collapse" id="p1">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p>
                      <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> O que é transtorno da atenção? </h3>
                        <p class="collapse" id="p2">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p> -->
                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Dr. Gustavo Sasdelli</h3>
                        <img src="/images/dr-gustavo-sasdeli.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="">veja mais</a>   
                        </p>
                        <hr>
                        <h3 class="sidebox-title">Doenças tratadas na Neurologia</h3>
                        <p> 
                            <ul>
                                <li> Parkinson </li>
                                <li> Tremor essencial </li>
                                <li> Convulsões </li>
                                <li> Alzheimer </li>
                                <li> Derrame cerebral (AVC) </li>
                                <li> Dores de cabeça </li>
                                <li> Meningite</li>
                                <li> Esclerose Múltipla </li>
                                <li> Doenças da medula espinhal </li>
                                <li> Dor neuropática </li>
                            </ul>
                        </p>
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')