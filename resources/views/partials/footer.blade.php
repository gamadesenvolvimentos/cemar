    <div id="footerOuterSeparator"></div>

    <div id="divFooter" class="footerArea">

        <div class="divPanel">

            <div class="row-fluid">
                <div class="span4" id="footerArea1">
                
                    <h3>Navegar</h3>
                    <div class="row-fluid">
                        <div class="span6">
                            
                            <p> 
                                <a href="./" title="Página Inicial">Home</a><br />
                                <a href="/aclinica" title="A Clínica">A Clínica</a><br />
                                <a href="/corpoclinico" title="">Corpo Clínico</a><br />
                                <a href="/convenios" title="">Convênios</a><br />
                            </p>
                        
                        </div>

                        <div class="span6">
                            
                            <p> 
                                <a href="/arquivos" title="">Artigos</a><br />
                                <a href="/contato" title="">Contatos</a><br />
                            </p>
                        
                        </div>
                        
                        <div class="row-fluid">
                            <div class="span12">
                                <p class="copyright">
                                    Copyright © 2015 Clínica CEMAR. Todos os direitos reservados.
                                </p>
                                <p class="social_bookmarks">
                                    <a href="https://www.facebook.com/cemarbarretos/?fref=ts" target="_blank"><i class="social foundicon-facebook"></i> Facebook</a>
                                </p>
                            </div>
                        </div>

                    </div>    

                </div>
                <div class="span4" id="footerArea2">

                    <h3>Especialidades</h3>
                    <div class="row-fluid">
                        <div class="span6"> 
                            <p> 
                                <a href="/clinica-urologia-cemar-barretos" title="">Urologia</a><br />
                                <a href="/clinica-dermatologia-cemar-barretos" title="">Dermatologia</a><br />
                                <a href="/clinica-otorrinolaringologia-cemar-barretos" title="">Otorrinolaringologia</a><br />
                            </p>
                        </div>
                        <div class="span6"> 
                                <p> 
                                    <a href="/clinica-reumatologia-cemar-barretos" title="">Reumatologia</a><br />
                                    <a href="/clinica-neurologia-cemar-barretos" title="">Neurologia</a><br />
                                    <a href="/clinica-endocrinologia-cemar-barretos" title="">Endocrinologia</a><br />
                                </p>
                        </div>
                    </div>        

                </div>
                <div class="span4" id="footerArea4">

                    <h3>Sugestões/Reclamações</h3>
                    <div class="btn-sugestao">
                           <p>
                               Nada melhor do que ouvi-los para melhorar a qualidade do nosso atendimento:
                           </p>
                           <a href="/sugestoes">
                            <i class="fa fa-smile-o"></i> registrar uma sugestão ou reclamação      
                           </a>
                    </div>
                    <div id="contact-info"> 
                        <span class="field">
                            <i class="fa fa-hospital-o"></i> Agende sua consulta:
                        </span>
                        <p> 
                            (17) 3322 6108 - (17) 99259 0742
                        </p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<br /><br /><br />

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<script src="scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/default.js" type="text/javascript"></script>

<script src="scripts/carousel/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script><script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script><script src="scripts/camera/scripts/camera.min.js" type="text/javascript"></script>
<script src="scripts/easing/jquery.easing.1.3.js" type="text/javascript"></script>

<script type="text/javascript">
    function startCamera() {
        $('#camera_wrap').camera({ 
            fx: 'scrollLeft', 
            time: 2000, 
            loader: 'none', 
            playPause: false, 
            navigation: true, 
            height: '35%', 
            pagination: true 
        });
    }
    $(function(){startCamera()});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox({
            openEffect  : 'elastic',
            closeEffect : 'elastic',

            helpers : {
                title : {
                    type : 'over'
                }
            }
        });
});
</script>

</body>
</html>