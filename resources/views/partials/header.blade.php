<!DOCTYPE HTML>
<html>
<head>
    <base href="/">
    <meta charset="utf-8">
    <title> <?php echo $GLOBALS['title']; ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="scripts/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="images/nav-icon.png">
    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

    <meta name="author" content="Gama Clínicas">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="scripts/icons/general/stylesheets/general_foundicons.css" media="screen" rel="stylesheet" type="text/css" />  
    <link href="scripts/icons/social/stylesheets/social_foundicons.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="scripts/carousel/style.css" rel="stylesheet" type="text/css" />
    <link href="scripts/camera/css/camera.css" rel="stylesheet" type="text/css" />

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>

    <link href="styles/custom.css" rel="stylesheet" type="text/css" />
    <link href="styles/carousel.css" rel="stylesheet" type="text/css" />

</head>
<body id="pageBody">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71230927-1', 'auto');
  ga('send', 'pageview');

</script>


<div id="divBoxed" class="container">

    <div class="transparent-bg" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: -1;zoom: 1;"></div>

    <div class="divPanel notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                        <img src="images/logo.png" />
                    </div>

                    <div id="divMenuRight" class="pull-right">
                        <div class="contact-header">
                           <div class="contact"> 
                               <div class="tel">
                                   <i class="fa fa-phone"></i> 17 3322 6108 <i class="fa fa-mobile"></i>  17 99259 0742
                               </div>
                               <div class="end">
                                   RUA 26, 951, CENTRO, BARRETOS-SP.
                               </div>
                           </div>
                           <div class="social">
                                <a href="https://www.facebook.com/cemarbarretos/?fref=ts" target="_blank">
                                    <i class="fa fa-facebook-official"></i>
                                </a>
                            </div>
                        </div>
                        <div class="navbar">
                            <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                                NAVIGATION <span class="icon-chevron-down icon-white"></span>
                            </button>
                            <div class="nav-collapse collapse">
                                <ul class="nav nav-pills ddmenu">
                                
                                    <li <?php if ($menu['active'] == 'home') echo "class='active'";?> ><a href="/">Home</a></li>
                                    
                                    <li <?php if ($menu['active'] == 'aclinica') echo "class='active'";?>><a href="/aclinica">A Clínica</a></li>
        							
                                    <li class="dropdown <?php if ($menu['active'] == 'especialidades') echo " active";?>">
                                        <a href="#" class="dropdown-toggle">Especialidades <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="/clinica-urologia-cemar-barretos"> Urologia </a></li>
                                            <li><a href="/clinica-dermatologia-cemar-barretos"> Dermatologia </a></li>
                                            <li><a href="/clinica-otorrinolaringologia-cemar-barretos"> Otorrinonaringologia </a></li>
                                            <li><a href="/clinica-reumatologia-cemar-barretos"> Reumatologia </a></li>
                                            <li><a href="/clinica-neurologia-cemar-barretos"> Neurologia </a></li>
                                            <li><a href="/clinica-endocrinologia-cemar-barretos"> Endocrinologia </a></li>
                                        </ul>
                                    </li>
                                    
                                    <li <?php if ($menu['active'] == 'corpoclinico') echo "class='active'";?>><a href="/corpoclinico">Corpo Clínico</a></li>
                                    
                                    <li <?php if ($menu['active'] == 'convenios') echo "class='active'";?>><a href="/convenios">Convênios</a></li>

                                    <li <?php if ($menu['active'] == 'artigos') echo "class='active'";?>><a href="/artigos">Artigos</a></li>
                                    
                                    <li <?php if ($menu['active'] == 'contato') echo "class='active'";?>><a href="/contato">Contato</a></li>
                                
                                </ul>
                                </div>
                        </div>
                    </div>

                </div>
            </div>