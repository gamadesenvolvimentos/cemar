<div style="width:500px; padding:20px; margin:0 auto; background-color:#F1EAF2;">
	<div style="padding-top:15px; padding-bottom:13px; font-size:18px; color:#634564; text-align:center;"> 
		<b>Olá, vocês receberam uma nova sugestão ou reclamação de {{$name}}</b>
	</div>
	<hr>
	<div style="padding-top:15px; padding-bottom:13px; font-size:18px; color:#634564; text-align:center;"> 
		<b>tipo: {{$tipo}}</b>
	</div>
	<hr>	
	<div style="padding-top:10px; color:#474848; text-align:justify; line-height:18px; font-size:14px;">
		<b> Mensagem: </b>
		<p>
			{{$mensagem}}
		</p>
	</div>
	<hr>	
	<div style="padding-top:10px; color:#474848; text-align:justify; line-height:18px; font-size:14px;">
		<b> Email do contato: </b> {{$email}} <br>
	</div>
</div>