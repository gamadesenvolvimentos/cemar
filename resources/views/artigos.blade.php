@include('partials.header')

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=173093449520884";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<hr>
<div class="contentArea">



    <div class="divPanel notop page-content">

        <div class="breadcrumbs">
            <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Artigos</span>
        </div>

        <div class="row-fluid">
         <!--Edit Main Content Area here-->
         <div class="span8" id="divMain">

            <h1>Artigos</h1>
            <hr>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/40436/NEUROLOGISTA-ORIENTA-SOBRE-SINTOMAS-E-TRATAMENTOS-PARA-CRISES-DE-ENXAQUECA" target="_blank"> Neurologista orienta sobre sintomas e tratamentos para crises de enxaqueca </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/40143/ENDOCRINOLOGISTA-ORIENTA-SOBRE-TIREOIDE-E-ENSINA-COMO-IDENTIFICAR-NODULOS-" target="_blank"> Endocrinologista orienta sobre tireoide e ensina como identificar nódulos  </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/39836/OTORRINOLARINGOLOGISTA-ORIENTA-SOBRE-ALERGIAS-RESPIRATORIAS-DURANTE-O-INVERNO" target="_blank"> Otorrinolaringologista orienta sobre alergias respiratórias durante o inverno  </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/39543/UROLOGISTA-BARRETENSE-ABORDA-OS-MITOS-E-VERDADES-SOBRE-O-CALCULO-RENAL-" target="_blank"> Urologista barretense aborda os mitos e verdades sobre o cálculo renal  </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/2135/CONHECA-SUA-PROSTATA" target="_blank"> Conheça sua próstata  </a>
                </div>
                <div class="veiculo">
                    Por: Gustavo Rocha - Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/1935/ENCARANDO-A-ANDROPAUSA" target="_blank"> Encarando a Andropausa  </a>
                </div>
                <div class="veiculo">
                    Por: Gustavo Rocha - Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/38975/DERMATOLOGISTA-ORIENTA-SOBRE-CUIDADOS-COM-A-PELE-DURANTE-O-INVERNO-" target="_blank"> Dermatologista orienta sobre cuidados com a pele durante o inverno  </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/3322/O-IMPACTO-DA-ACNE-" target="_blank"> O impacto da acne  </a>
                </div>
                <div class="veiculo">
                    Por Cristina Alessi - Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/2530/MANCHAS-NO-ROSTO-CAUSAS-E-TRATAMENTO" target="_blank"> Manchas no rosto: causas e tratamento </a>
                </div>
                <div class="veiculo">
                    Por Cristina Alessi - Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/2248/CAPSULAS-DE-BELEZA" target="_blank"> Cápsulas de beleza </a>
                </div>
                <div class="veiculo">
                    Por Cristina Alessi - Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/39252/MEDICA-ORIENTA-SOBRE-AGRAVAMENTO-DE-REUMATISMO-DURANTE-O-PERIODO-DO-INVERNO" target="_blank"> Médica orienta sobre agravamento de reumatismo durante o período do inverno </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>

            <div class="artigo-item">
                <div class="title">
                    <a href="http://www.odiarioonline.com.br/noticia/18147/CLINICA-GERAL-ESCLARECE-MUDANCAS-NO-ORGANISMO-DURANTE-HORARIO-DE-VERAO" target="_blank"> Clínica geral esclarece mudanças no organismo durante horário de verão </a>
                </div>
                <div class="veiculo">
                    Veículo: O Diário Online
                </div>
            </div>


        </div>
        <!--End Main Content Area here-->

        <!--Edit Sidebar Content here-->
        <div class="span4 sidebar">

            <div class="sidebox">
                <h3 class="sidebox-title">Curta nossa página no Facebook</h3>
                <div class="fb-page" data-href="https://www.facebook.com/cemarbarretos/?fref=ts" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/cemarbarretos/?fref=ts"><a href="https://www.facebook.com/cemarbarretos/?fref=ts">CEMAR</a></blockquote></div></div>




            </div>

        </div>
        <!--End Sidebar Content here-->
    </div>

    <div id="footerInnerSeparator"></div>
</div>
</div>


@include('partials.footer')