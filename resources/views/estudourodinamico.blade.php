@include('partials.header')
    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
               Estudo Urodinâmico
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span><a href="/clinica-urologia-cemar-barretos">urologia</a> &nbsp;/&nbsp; estudo urodinâmico </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        O estudo urodinâmico é um exame solicitado para avaliação de distúrbios da micção em homens e muheres. Esse exame é importante para auxiliar o médico a diagnosticar doenças do trato urinário inferior (bexiga, esfíncter , uretra e próstata) e definir o melhor tratamento para cada caso. Além disso, é um exame que também pode ser decisivo quanto à indicação ou não de uma cirurgia em função dos resultados.
                    </p>
                    <p>
                        O exame consiste na introdução de um fino cateter no interior da bexiga. Esse cateter possui dois canais; um é utilizado para encher a bexiga com soro fisiológico e o outro é conectado a um computador em que o médico acompanha tudo que está acontecendo com a bexiga e esfíncteres. Ao final do enchimento da bexiga o médico irá solicitar que você urine normalmente para esvaziá-la.
                    </p>
                    <p>
                        Geralmente, o laudo do exame fica pronto logo em seguida.
                    </p>

                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">

                        <h3 class="sidebox-title">Dr. Gustavo Rocha</h3>
                        <img src="/images/dr-gustavo.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="/corpoclinico">veja mais</a>   
                        </p>
                        <hr>

                        <a href="/files/PREPARO_ESTUDO_URODINAMICO.pdf" target="_blank">                        
                            <div class="bot-procedimentos">
                                <div class="row-fluid" style="padding-top:7px;">
                                    <div class="span3 icon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                    <div class="span9">
                                        <div class="desc">
                                            Preparo Estudo Urodinâmico
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>      
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')