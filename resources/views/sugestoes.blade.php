@include('partials.header')

    <hr>
    <div class="contentArea">

    <div class="head-sugestoes">
        
    </div>

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Sugestões e Reclamações</span>
            </div>

            <div class="row-fluid">
                <div class="span8" id="divMain">

                    <h1>Sugestões e Reclamações</h1>
                    {!! csrf_field() !!}
                          
                      <div class="success" {!! (session('success')) ? 'style="display:block;"' : '' !!}> 
                        E-mail enviado com sucesso! 
                        <br>
                        <strong>
                            É um prazer ouvi-lo(a)! Vamos trabalhar para que possamos ser cada vez melhores! 
                        </strong> 
                      </div>
                    
                    <hr>
                    <!--Start Contact form -->                                                      
                    <form name="enq" id="contact-form" method="post" action="/sugestoes">
                      <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="radio" name="tipo" class="radio" value="sugestao" checked=""> Sugestão </option> <br>
                        <input type="radio" name="tipo" value="reclamacao"> Reclamação </option>
                        <hr>
                        
                        <input type="text" name="name" id="name" value=""  class="input-block-level" placeholder="Digite seu nome" />
                        <input type="text" name="email" id="email" value="" class="input-block-level" placeholder="Digite seu e-mail" />
                        <textarea rows="11" name="mensagem" id="message" class="input-block-level" placeholder="Comentario"></textarea>
                        <div class="actions">
                        <input type="submit" value="Enviar" name="submit" id="submitButton" class="btn btn-info pull-right" title="Clique aqui para enviar sua sugestão ou rteclamação" onclick="javascript:document.getElementById('contact-form').submit();" />
                        </div>
                        
                        </fieldset>
                    </form>                  
                    <!--End Contact form -->                                             
                </div>
                
                <!--Edit Sidebar Content here-->    
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Informações para contato</h3>
                    <p>
                        <strong> Telefones: </strong> <br>
                        (17) 3322 6108 <br> (17) 99259 0742<br>
                        <hr>
                        <strong> Endereço: </strong> <br>
                        Rua 26, 951, Centro, Barretos-SP  
                    </p>     
                     
                     <!-- Start Side Categories -->
        
                    <!-- End Side Categories -->
                                        
                    </div>
       
                </div>
            <!--/End Sidebar Content-->
                
                
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')