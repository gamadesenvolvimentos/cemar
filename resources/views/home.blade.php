@include('partials.header')
            <div class="row-fluid">
            <div class="span12">

                <div id="headerSeparator"></div>

                <div class="camera_full_width">
                    <div id="camera_wrap">
                        <div data-src="slider-images/1.jpg" >
                        </div>
                        <div data-src="slider-images/2.jpg" >
                        </div>
                        <div data-src="slider-images/3.jpg" >                 
                        </div>
                    </div>
                    <br style="clear:both"/><div style="margin-bottom:40px"></div>
                </div>               

                <div id="headerSeparator2"></div>

            </div>
        </div>
    </div>

    <div class="contentArea">

        <div class="divPanel notop page-content">
            

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span12" id="divHome">

                    <div class="row-fluid">
                         <h4><i class="fa fa-user-md"></i> ESPECIALIDADES DA CLÍNICA CEMAR</h4>
                        <hr>
                    </div>
            
                    <div class="row-fluid">
                        <div class="span4">
                            <a href="/clinica-urologia-cemar-barretos">
                                <div class="especialidade-home bg-urologista-home">
                                    <h3>
                                        Urologista
                                        <span>Dr. Gustavo Rocha</span>
                                    </h3>
                                </div>
                            </a>
                        </div>
                        <div class="span4">
                            <a href="/clinica-dermatologia-cemar-barretos">
                                <div class="especialidade-home bg-dermato-home">
                                    <h3>
                                        Dermatologista
                                        <span>Dra. Cristina Alessi</span>
                                    </h3>
                                </div>  
                            </a>
                        </div>
                        <div class="span4">
                            <a href="/clinica-otorrinolaringologia-cemar-barretos">
                                <div class="especialidade-home bg-otorrino-home">
                                    <h3>
                                        Otorrinolaringologista
                                        <span>Dr. Fransérgio Cavallari</span>
                                    </h3>
                                </div> 
                            </a>
                        </div>
                    </div>

                    <div class="row-fluid" style="margin-top:18px;">
                        <div class="span4">
                            <a href="/clinica-reumatologia-cemar-barretos">
                                <div class="especialidade-home bg-reumato-home">
                                    <h3>
                                        Reumatologista
                                        <span>Dra. Carolina Sasdelli</span>
                                    </h3>
                                </div>
                            </a> 
                        </div>
                        <div class="span4">
                            <a href="/clinica-neurologia-cemar-barretos">
                                <div class="especialidade-home bg-neuro-home">
                                    <h3>
                                        Neurologista
                                        <span>Dr. Gustavo Sasdelli </span>
                                    </h3>
                                </div>
                            </a> 
                        </div>
                        <div class="span4">
                            <a href="/clinica-endocrinologia-cemar-barretos">
                                <div class="especialidade-home bg-endocrino-home">
                                    <h3>
                                        Endocrinologista
                                        <span>Dra. Andreza Vargas</span>
                                    </h3>
                                </div>
                            </a> 
                        </div>
                    </div>

    <div class="row-fluid home-itens">
        
        <div class="span4">
                <div class="box">
                    <h4 class="title"><i class="fa fa-heartbeat"></i> <br> Atendimento Humanizado</h4> <hr/>
                    <p> 
                       A missão do Centro Médico Alessi e Rocha  é preservar a saúde e a qualidade de vida das pessoas, a partir de um atendimento ético, humano e personalizado com base no conhecimento científico atual
                    </p>
                </div>
        </div> 
            
        <div class="span4">
                <div class="box">
                    <h4 class="title"> <i class="fa fa-hospital-o"></i> <br> Clínica Médica em Barretos</h4> <hr/>
                    <p>
                         Podemos oferecer para população de Barretos e região o que há de melhor na área da saúde com atendimento humanizado e todo conforto merece...
                         <br>
                    </p>
                    <a href="/aclinica">Conheça mais sobre a clínica &raquo;</a>
                </div>
        </div> 
            
        <div class="span4">
                <div class="box">
                    <h4 class="title"><i class="fa fa-clock-o"></i> <br> Horários de Funcionamento</h4> <hr/>
                    <p> 
                        <h4 class="time-home">
                        SEGUNDA À SEXTA <br>
                        07:30 ás 19:00
                        </h4> 
                    </p>
                </div>
        </div> 

    </div>

                </div>
            <!--End Main Content-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>
@include('partials.footer')