@include('partials.header')

    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Otorrinolariongologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>otorrinolariongologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        <img src="/images/otorrino.png">
                        
                        A otorrinolaringologia é uma especialidade complexa, pois compreende múltiplas subespecialidades e pacientes adultos e pediátricos. Dentre elas, destacamos : 
                        <br>
                        <br>
                        <b> Rinossinusologia </b>: doenças do nariz e dos seios paranasais. Inclui sinusites, rinites alérgicas, desvios de septo e pólipos nasais.
                        <br>
                        <br>
                        <b> Otologia </b>: doenças do ouvido e do labirinto. Compreende as infecções do ouvido, surdez, zumbido (barulho no ouvido), tímpano perfurado e tonturas (labirintite).
                        <br>
                        <br>
                        <b> Laringologia </b>: doenças da voz.
                        <br>
                        <br>
                        <b> Faringoestomatologia </b>: doenças da boca e das amígdalas.
                        <br>
                        <br>
                        <b> Ronco e Apnéia </b>: parte da Medicina do Sono que compreende as causas mecânicas de ronco e apneia obstrutiva do sono. 
                    </p>
                    <p>
                        A especialidade apresenta muitos desafios, desde a parte clínica até a parte de exames e cirurgias. 
                    </p>
                    <p>    
                        Hoje, a videocirurgia de nariz e seios da face é realizada por muitos otorrinolaringologistas no país, e é um método muito menos invasivo que os meios tradicionais de se operar, e todas as cirurgias deste tipo são realizadas pelo otorrinolaringologista da clínica. 
                    </p>
                    <p>
                        A cirurgia de ouvido via videoendoscopia é uma realidade muito recente no país, sendo feita em poucos centros no Brasil. É pouco invasiva, raramente exige cortes na pele fora do canal do ouvido e tem recuperação pós operatória muito mais rápida que a cirurgia convencional. Essa cirurgia também é feita pelo Dr Fransergio.
                    </p>
                    <p>    
                        Clinicamente, as alergias respiratórias representam um desafio à parte aos otorrinolaringologistas, uma vez que no inverno de nossa região, todos sofrem com o clima seco e o controle dos sintomas alérgicos é uma fonte frequente de procura pela especialidade.
                    </p>
                    <p>    
                        Tonturas, zumbido e surdez são problemas recorrentes na especialidade. A população idosa está aumentando, e com ela a frequência de consultas devido a perda auditiva também se eleva. O uso crescente de fones de ouvido aumenta os casos de perda auditiva em pacientes mais jovens, e a prevenção desses casos também é um desafio importante da otorrinolaringologia.

                    </p>
                    <br>
                    <h3><i class="fa fa-question-circle"></i> Perguntas frequentes sobre otorrinolaringologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> A pessoa tem Sinusite ou está com Sinusite? </h3>
                        <p class="collapse" id="p1">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p>
                      <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> Meu nariz vive entupido. O que pode ser? </h3>
                        <p class="collapse" id="p2">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p>
                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Dr. Fransérgio Cavallari</h3>
                        <img src="/images/dr-fransergio.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="">veja mais</a>   
                        </p>
                        <hr>
                        <h3 class="sidebox-title">Doenças tratadas na Otorrinolaringologia</h3>
                        <p class="doencas"> 
                            <div class="doencas-list" id="doencas">   
                                <ul>
                                    <li>Amigdalites</li>
                                    <li>Faringites</li>
                                    <li>Laringites</li>
                                    <li>Aftas</li>
                                    <li>Doenças da língua</li>
                                    <li>Doenças das glândulas salivares</li>
                                    <li>Doenças da voz</li>
                                    <li>Otites</li>
                                    <li>Perfuração do tímpano</li>
                                    <li>Colesteatoma (tumor benigno do ouvido e do osso do ouvido)</li>
                                    <li>Tonturas (labirintite)</li>
                                    <li>Surdez</li>
                                    <li>Nariz entupido - desvios de septo, aumento de adenóides, pólipos nasais</li>
                                    <li>Sinusites</li>
                                    <li>Rinite alérgica</li>
                                    <li>Orelhas de abano</li>
                                    <li>Deformidades do nariz</li>
                                    <li>Ronco e apnéia do sono</li>
                                </ul>
                            </div>    
                            <div class="doencas-more" onclick="showdoencas();" id="showdoencas">
                            mostrar mais doenças<br>
                            <i class="fa fa-caret-down"></i>
                            </div>
                            <div class="doencas-less" onclick="hidedoencas();" id="hidedoencas">
                                <i class="fa fa-caret-up"></i><br>
                                mostrar menos doenças
                            </div>

                            <a href="{{ URL::to( '/files/orientacao-pos-cirurgia-de-nariz.pdf')  }}" target="_blank">                   
                                <div class="bot-procedimentos">
                                    <div class="row-fluid">
                                        <div class="span3 icon" style="padding-top:7px;">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                        <div class="span9" style="padding-top:0px;">
                                            <div class="desc">
                                                Orientação pós cirúrgica de nariz
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            
                            <a href="{{ URL::to( '/files/orientacao-pos-amigdala.pdf')  }}" target="_blank">                   
                                <div class="bot-procedimentos">
                                    <div class="row-fluid">
                                        <div class="span3 icon" style="padding-top:7px;">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                        <div class="span9" style="padding-top:0px;">
                                            <div class="desc">
                                                Orientação pós Amigdala
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <a href="{{ URL::to( '/files/orientacao-pos-cirurgia-de-OUVIDO.pdf')  }}" target="_blank">                   
                                <div class="bot-procedimentos">
                                    <div class="row-fluid">
                                        <div class="span3 icon" style="padding-top:7px;">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                        <div class="span9" style="padding-top:0px;">
                                            <div class="desc">
                                                Orientação pós cirúrgica de ouvido
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <a href="{{ URL::to( '/files/orientacao-pre-op.pdf')  }}" target="_blank">                   
                                <div class="bot-procedimentos">
                                    <div class="row-fluid">
                                        <div class="span3 icon" style="padding-top:7px;">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                        <div class="span9" style="padding-top:0px;">
                                            <div class="desc">
                                                Orientação pré operatórias
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>


                        </p>
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>

<script>

    function showdoencas() {
        
        document.getElementById("doencas").style.height = "100%";
        document.getElementById("showdoencas").style.display = "none";
        document.getElementById("hidedoencas").style.display = "block";

    } 

    function hidedoencas() {
        document.getElementById("hidedoencas").style.display = "none";
        document.getElementById("showdoencas").style.display = "block";
        document.getElementById("doencas").style.height = "300px";
    }

</script>

@include('partials.footer')