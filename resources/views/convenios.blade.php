@include('partials.header')

    <hr>
    <div class="contentArea">


        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Convênios</span>
            </div>

            <!-- Carousel -->
                <div class='row-fluid'>
                    <div class='span12'>
                      <div class="carousel slide media-carousel" id="media">

                        <div class="carousel-inner">
                          
                          <div class="item active">
                            
                            <div class="row-fluid">

                              <div class="span2">
                                <a class="thumbnail" href="http://www.apasbarretos.com.br/" target="_blank"><img alt="" src="images/convenios/apas-barretos.jpg"></a>
                              </div>          
                              <div class="span2">
                                <a class="thumbnail" href="http://www.bradescosaude.com.br" target="_blank"><img alt="" src="images/convenios/bradesco.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="http://www.cabesp.com.br/" target="_blank"><img alt="" src="images/convenios/cabesp.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="http://www.cassi.com.br" target="_blank"><img alt="" src="images/convenios/cassi.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="http://www.economus.com.br" target="_blank"><img alt="" src="images/convenios/economus.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="http://www.prevcesp.com.br/wps/portal" target="_blank"><img alt="" src="images/convenios/funcesp.jpg"></a>
                              </div>

                            </div>
                          
                          </div>

                          <div class="item">
                            
                            <div class="row-fluid">
                              <div class="span2">
                              </div>

                              <div class="span2">
                                <a class="thumbnail" href="http://www.unimed.com.br" target="_blank"><img alt="" src="images/convenios/unimed.jpg"></a>
                              </div>          
                              <div class="span2">
                                <a class="thumbnail" href="http://www.saofrancisco.com.br/saude" target="_blank"><img alt="" src="images/convenios/saofrancisco.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="http://www.alimentacaobarretos.org.br/" target="_blank"><img alt="" src="images/convenios/sindicato-alimentacao-barretos.jpg"></a>
                              </div>
                              <div class="span2">
                                <a class="thumbnail" href="#" target="_blank"><img alt="" src="images/convenios/sulamerica.jpg"></a>
                              </div>
                              
                              

                            </div>
                          
                          </div>
                          
                          
                        </div>

                          <a data-slide="next" href="#media" class="right carousel-control">›</a>
                          <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
                      </div>
                    </div>
                </div>                            
                <!-- Carousel -->

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span12" id="divMain">

                    <h1>Convênios</h1>
					Confira os convênios que atendemos em cada especialidade
                    <hr>

                    <br>					 
			     
                </div>

            </div>

            <div class="row-fluid">
            
                <div class="span4 convenio-especialidade">
                    <h3>Urologia</h3>
                    <span>Dr. Gustavo Rocha</span>
                    <ul>
                        <li><a href="/contato" >Particular</a></li>
                        <li><a href="http://www.unimed.com.br">Unimed</a></li>
                        <li><a href="http://www.cabesp.com.br/" target="_blank">CABESP</a></li>
                        <li><a href="http://sulamericasaudesa.com.br/" target="_blank">Sul America</a></li>
                        <li><a href="http://www.bradescosaude.com.br" target="_blank">Bradesco</a></li>
                        <li><a href="http://www.prevcesp.com.br/wps/portal" target="_blank">Fundação CESP</a></li>
                        <li><a href="http://www.saofrancisco.com.br/saude" target="_blank">São Francisco Saúde</a></li>
                        <li><a href="http://www.cassi.com.br" target="_blank">Cassi</a></li>
                        <li><a href="http://www.economus.com.br" target="_blank">Economus</a></li>
                        <li><a href="http://www.apasbarretos.com.br/" target="_blank">APAS</a></li>
                        <li><a href="/convenios">AMAS</a></li>
                        <li><a href="#">Vale Saúde</a></li>
                        <li><a href="http://www.alimentacaobarretos.org.br/" target="_blank">Sindicato Alimentação</a></li>
                    </ul>    
                </div>
                <div class="span4 convenio-especialidade">
                    <h3>Dermatologia</h3>
                    <span>Dra. Cristina Alessi</span>
                    <ul>
                        <li><a href="/contato" >Particular</a></li>
                        <li><a href="http://www.cabesp.com.br/" target="_blank">CABESP</a></li>
                        <li><a href="http://sulamericasaudesa.com.br/" target="_blank">Sul America</a></li>
                        <li><a href="http://www.bradescosaude.com.br" target="_blank">Bradesco</a></li>
                        <li><a href="http://www.prevcesp.com.br/wps/portal" target="_blank">Fundação CESP</a></li>
                        <li><a href="http://www.cassi.com.br" target="_blank">Cassi</a></li>
                        <li><a href="http://www.economus.com.br" target="_blank">Economus</a></li>
                        <li><a href="http://www.apasbarretos.com.br/" target="_blank">APAS</a></li>
                        <li><a href="/convenios">AMAS</a></li>
                        <li><a href="/convenios">Vale Saúde</a></li>
                        <li><a href="http://www.alimentacaobarretos.org.br/" target="_blank">Sindicato Alimentação</a></li>
                    </ul>    
                </div>
                <div class="span4 convenio-especialidade">
                    <h3>Reumatologia</h3>
                    <span>Dra. Carolina Sasdelli </span>
                    <ul>
                        <li><a href="/contato" >Particular</a></li>
                        <li><a href="/convenios" target="_blank">AMAS</a></li>
                    </ul>    
                </div>
            
            </div>

            <div class="row-fluid">
            
                <div class="span4 convenio-especialidade">
                    <h3>Otorrinolaringologia</h3>
                    <span>Dr. Fransérgio Cavallari </span>
                    <ul>
                        <li><a href="/contato" >Particular </a></li>
                        <li><a href="http://www.unimed.com.br">Unimed</a></li>
                        <li><a href="http://www.santacasabarretos.com.br/" target="_blank">Santa Casa Saúde </a></li>
                        <li><a href="/convenios">AMAS </a></li>
                        <li><a href="http://www.apasbarretos.com.br/" target="_blank">APAS</a></li>                        
                    </ul>    
                </div>
                <div class="span4 convenio-especialidade">
                    <h3>Endocrinologia</h3>
                    <span>Dra. Andreza Vargas  </span>
                    <ul>
                        <li><a href="/contato" >Particular </a></li>
                        <li><a href="/convenios">AMAS </a></li>
                        <li><a href="http://www.apasbarretos.com.br/" target="_blank">APAS</a></li>                        
                    </ul>    
                </div>
                <div class="span4 convenio-especialidade">
                    <h3>Neurologia</h3>
                    <span>Dr. Gustavo Sasdelli  </span>
                    <ul>
                        <li><a href="/contato" >Particular</a></li>
                        <li><a href="/convenios" target="_blank">AMAS</a></li>
                    </ul>    
                </div>
            
            </div>


            <div id="footerInnerSeparator"></div>
        </div>

    </div>

@include('partials.footer')