@include('partials.header')
    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Reumatologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>reumatologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        <img src="/images/reumatologia.png">
                        Diagnosticar doenças do sistema musculoesquelético (articulações, ossos, tendões e tecido conjuntivo) e doenças autoimunes e evitar ou retardar deformidades, com atenção especial às dores crônicas, melhorando dessa forma a qualidade de vida.            
                        <br>  
                        <br>  
                        <br>  
                        <br>  
                        <br>  
                        <br>  
                        <br>  
                        <br>  
                    </p>
                    <br>
                    <!-- <h3><i class="fa fa-question-circle"></i> Perguntas frequentes sobre reumatologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> O que significa exames FATOR REUMATÓIDE? Significa a presença de algum reumatismo?  </h3>
                        <p class="collapse" id="p1">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p>
                      <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> Qual o valor normal para o FATOR REUMATÓIDE? A partir de quanto ele determina doença?  </h3>
                        <p class="collapse" id="p2">
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                        </p> -->
                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Dra. Carolina Sasdelli</h3>
                        <img src="/images/dra-carolina.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="">veja mais</a>   
                        </p>
                        <hr>
                        <h3 class="sidebox-title">Doenças tratadas na Reumatologia</h3>
                        <p> 
                            <ul>
                                <li> Artrite Reumatóide </li>
                                <li> Artrite Reumatóide Juvenil </li>
                                <li> Doença de Behçet </li>
                                <li> Esclerodermia </li>
                                <li> Espondiloartropatias Soronegativas </li>
                                <li> Gota </li>
                                <li> Lombalgias </li>
                                <li> Osteoartrite (Artrose)</li>
                                <li> Polimialgias </li>
                                <li> Pseudo-gota</li>
                                <li> Reumatismos de partes moles (tendinites, bursites e entesites)</li>
                                <li> Síndrome Anti-fosfolípide </li>
                                <li> Síndrome de Sjögren </li>
                                <li> Vasculites </li>
                            </ul>
                        </p>
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')