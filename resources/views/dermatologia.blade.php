@include('partials.header')

    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Dermatologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>dermatologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p class="especialidade-desc">
                        <img src="/images/dermatologia.png">
                        
                        A dermatologia é uma especialidade médica multidisciplinar, responsável pelo diagnóstico, pela prevenção e pelo tratamento de doenças relacionadas a pele, mucosas, cabelos e unhas de adultos e também de crianças e idosos. 
                        <br>
                        A preocupação do dermatologista transcende aspectos puramente médicos; navega também pelos aspectos do bem-estar, da qualidade de vida e da autoestima dos pacientes.
                        <br>
                        <br>
                        Dermatologia clínica, cirúrgica e estética. 
                        <!-- <br><br>   
                        O principal desafio hoje é a valorização da especialidade, pois muitos médicos que não são dermatologistas e até mesmo não médicos se julgam capazes de realizar nossos procedimentos, acarretando riscos aos pacientes e diminuindo o risco que os envolve. -->                    
                    </p>
                    <p>
                        <h5> Dermatologia Clínica </h5>
                        Prevenção de doenças, como check up anual contra o câncer o de pele, o tipo de câncer mais comum no Brasil. Prevenção de dermopatias atópicas, alopecia androgenética, envelhecimento cutâneo e flacidez corporal.
                        <div class="especialidade-list">
                            <strong>Diagnóstico e tratamento de:</strong>
                            <br>
                            <ul>
                                <li>Acne juvenil e do adulto</li>
                                <li>Rosácea</li>
                                <li>Melasma</li>
                                <li>Onicomicose e doenças das unhas</li>
                                <li>Infecções bacterianas, fúngicas ou virais da pele de mucosas</li>
                                <li>Dermatites</li>
                                <li>Queda de cabelos e outras afecções do couro cabeludo</li>
                                <li>Doenças autoimunes: psoríase e vitiligo</li>
                                <li>Ceratoses actínicas e seborreicas</li>
                                <li>Urticária</li>
                                <li>Hiper-hidrose</li>
                            </ul>
                        </div>    

                        <h5>Dermatologia cirúrgica</h5>
                        <div class="especialidade-list">
                            <strong>Remoção cirúrgica (exérese ou eletrocoagulação) de:</strong>
                            <ul>
                                <li>Nevos (pintas)</li>
                                <li>Cistos e lipomas</li>
                                <li>Verrugas</li>
                                <li>Tumores de pele: benignos ou malignos (papilomas, carcinomas, melanomas e outros) </li>
                            </ul>
                        </div>

                        <h5>Dermatologia Estética</h5>
                        <div class="especialidade-list">
                            <ul>
                                <li>Botox: face, pescoço e axilas (para hiper-hidrose)</li>
                                <li>Preenchimento com ácido hialurônico: face, colo, orelhas e mãos</li>
                                <li>Peelings: químicos </li>
                                <li>Laser para manchas (sardas e melasma)</li>
                                <li>Laser para vasinhos (telangiectasias)</li>
                                <li>Laser para rejuvenescimento</li>
                            </ul>
                        </div>        
                    </p>

                    <h3><i class="fa fa-question-circle"></i> Perguntas frequentes sobre dermatologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> Fatos e Casos de Verão: Fitofotodermatose  </h3>
                        <p class="collapse" id="p1">
                           No verão, é comum que as pessoas peguem sol com resíduos de frutas cítricas nas mãos ou em outras partes do corpo, gerando lesões chamadas fitofotodermatoses. As frutas que “mancham” a pele são: limão, caju e figo.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> Filtro Solar e Cabelos  </h3>
                        <p class="collapse" id="p2">
                          Não é só a pele que sofre com os raios solares, mas também os cabelos. Por isso é recomendado que se use produtos com filtro solar específicos para cabelos, presentes na forma de mousse ou loção capilar.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p3" class="pergunta"> Tem idade permitida para se iniciar o uso de filtro solar?  </h3>
                        <p class="collapse" id="p3">
                          Sim, os filtros solares, mesmo específicos para crianças, só podem ser usados após 6 meses de vida.

                        </p>
                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Dra. Cristina Alessi</h3>
                        <img src="/images/dr-cristina.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="/corpoclinico/dra-cristina-alessi">veja mais</a>   
                        </p>
                        <hr>
                        <!-- <h3 class="sidebox-title">Doenças tratadas na Dermatologia</h3>
                        <p> 
                            <ul>
                                <li> Acne </li>
                                <li> Caspa </li>
                                <li> Câncer de pele </li>
                                <li> Celulite </li>
                                <li> Dermatite </li>
                                <li> Descamações </li>
                                <li> Dermatopatias </li>
                                <li> Eczema </li>
                            </ul>
                        </p> -->
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')