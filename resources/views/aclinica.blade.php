@include('partials.header')

    <hr>
    <div class="contentArea">

    <div class="head-about">
        
    </div>

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="./">Home</a> &nbsp;/&nbsp; <span>A Clínica</span>
            </div>

            <div class="row-fluid">
			<!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <h1>A Clínica</h1>
					<hr>
                    <p>
                        Inaugurada em fevereiro de 2015, o centro médico CEMAR foi planejado para responder a uma crescente necessidade de modernização na área da saúde de Barretos e região. A figura de uma clínica de instalações modernas e confortáveis, com atendimento ágil e humanizado, foi o objetivo de nosso planejamento. Os profissionais deveriam ser competentes e comprometidos com sua atualização permanente. Hoje, temos a certeza de que nossos pacientes encontram atendimento compatível com o oferecido em centros maiores, tanto em conforto como, e principalmente, na excelência profissional do corpo clínico que incessantemente busca o que há de mais moderno a ser oferecido em cada área.
                    </p>
                    <br>
                    <h3>Estrutura</h3>
                    <hr>
                    <p>
                        O CEMAR conta com uma ampla recepção, climatizada, com televisão, jardim de inverno e dois banheiros acessíveis  (masculino e feminino). Jornais, revistas atualizadas, água e café estão disponíveis. <br>
                        Dispomos de cinco consultórios médicos, uma sala de procedimentos, uma sala de exames (estudo urodinâmico), uma sala de administração, copa e banheiros para os funcionários, salas de expurgo e de esterilização de materiais.
                    </p>
                    <div class="row-fluid img-galery">
                    
                        <div class="span3">
                            <a href="images/album/0.jpg" class="fancybox" rel="gallery" title="Recepção">
                                <img src="images/album/thumb/0.jpg" alt="">
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/1.jpg" class="fancybox" rel="gallery" title="Banheiros Masculino e Feminino">
                                <img src="images/album/thumb/1.jpg" alt=""> 
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/2.jpg" class="fancybox" rel="gallery" title="Sala de espera">
                                <img src="images/album/thumb/2.jpg" alt=""> 
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/3.jpg" class="fancybox" rel="gallery" title="Sala de espera">
                                <img src="images/album/thumb/3.jpg" alt=""> 
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid img-galery">
                        
                        <div class="span3">
                            <a href="images/album/4.jpg" class="fancybox" rel="gallery" title="Sala da Dra. Cristina Alessi">
                                <img src="images/album/thumb/4.jpg" alt="">
                            </a>    
                        </div>
                        <div class="span3">
                            <a href="images/album/5.jpg" class="fancybox" rel="gallery" title="Sala da Dra. Cristina Alessi">
                                <img src="images/album/thumb/5.jpg" alt="">
                            </a> 
                        </div>
                        <div class="span3">
                            <a href="images/album/6.jpg" class="fancybox" rel="gallery" title="Sala de Procedimentos">
                                <img src="images/album/thumb/6.jpg" alt="">
                            </a> 
                        </div>
                        <div class="span3">
                            <a href="images/album/7.jpg" class="fancybox" rel="gallery" title="Sala de Urodinâmica">
                                <img src="images/album/thumb/7.jpg" alt="">
                            </a> 
                        </div>

                    </div>
                    <div class="row-fluid img-galery">
                        
                        <div class="span3">
                            <a href="images/album/8.jpg" class="fancybox" rel="gallery" title="Administração">
                                <img src="images/album/thumb/8.jpg" alt="">
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/9.jpg" class="fancybox" rel="gallery" title="Sala da Dra. Andreza Vargas">
                                <img src="images/album/thumb/9.jpg" alt="">
                            </a> 
                        </div>
                        <div class="span3">
                            <a href="images/album/10.jpg" class="fancybox" rel="gallery" title="Sala da Dra. Andreza Vargas">
                                <img src="images/album/thumb/10.jpg" alt="">
                            </a> 
                        </div>
                        <div class="span3">
                            <a href="images/album/18.jpg" class="fancybox" rel="gallery" title="">
                                <img src="images/album/thumb/18.jpg" alt="Sala da Dra. Andreza Vargas">
                            </a>
                        </div>

                    </div>                   
             
                    <div class="row-fluid img-galery">
                        <div class="span3">
                            <a href="images/album/11.jpg" class="fancybox" rel="gallery" title="Sala do Dr. Gustavo Rocha">
                                <img src="images/album/thumb/11.jpg" alt="">
                            </a> 
                        </div>

                        <div class="span3">
                            <a href="images/album/12.jpg" class="fancybox" rel="gallery" title="Sala do Dr. Gustavo Rocha">
                                <img src="images/album/thumb/12.jpg" alt="">
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/13.jpg" class="fancybox" rel="gallery" title="Sala do Dr. Gustavo Rocha">
                                <img src="images/album/thumb/13.jpg" alt="">
                            </a> 
                        </div>
                        <div class="span3">
                            <a href="images/album/14.jpg" class="fancybox" rel="gallery" title="Sala do Dr. Gustavo Sasdelli e da Dra. Carolina Sasdelli">
                                <img src="images/album/thumb/14.jpg" alt="">
                            </a>
                        </div>
                        

                    </div>               
                    <div class="row-fluid img-galery">
                        
                        <div class="span3">
                            <a href="images/album/15.jpg" class="fancybox" rel="gallery" title="Sala Dr. Fransérgio Cavallari ">
                                <img src="images/album/thumb/15.jpg" alt="">
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/16.jpg" class="fancybox" rel="gallery" title="Sala Dr. Fransérgio Cavallari ">
                                <img src="images/album/thumb/16.jpg" alt="">
                            </a>
                        </div>
                        <div class="span3">
                            <a href="images/album/17.jpg" class="fancybox" rel="gallery" title="Sala Dr. Fransérgio Cavallari ">
                                <img src="images/album/thumb/17.jpg" alt="">
                            </a> 
                        </div>
                        

                    </div>					 
			 
                </div>
				<!--End Main Content Area here-->
				
				<!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Horários de Funcionamento</h3>
                        <p>
                            Segunda à Sexta: 07:30 às 19:00 <br>
                            
                        </p>
                        <h3 class="sidebox-title">Missão</h3>
                        <p>
                            Preservar a saúde e a qualidade de vida das pessoas, a partir de um atendimento ético, humano, personalizado e com base no conhecimento científico atual.
                        </p>
                        <h3 class="sidebox-title">Visão</h3>
                        <p>
                            Ser reconhecida em Barretos e região pela excelência, liderança e pioneirismo em assistência à saúde.
                        </p>
                        <h3 class="sidebox-title">Valores</h3>
                        <p>
                            Ética, Humanização, Respeito, Comprometimento e Responsabilidade
                        </p>
                                      

                                            
				   			
                    </div>
                    
                </div>
				<!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>

@include('partials.footer')