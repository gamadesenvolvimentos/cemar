@include('partials.header')
    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Corpo Clínico</span>
            </div>

             <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span12" id="divMain">
                    <h1> Corpo Clínico </h1>
                    <hr>
                </div>
            </div>        

            <div class="row-fluid">
			<!--Edit Main Content Area here-->

                <div class="span6">

                    <div class="corpo-clinico-item">
                        <div class="row-fluid">
                            <div class="span5">
                                <img src="/images/dr-cristina.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dra. Cristina Alessi </h3>
                                <div class="especialidade"> Dermatologista <div class="pull-right"><a href="http://lattes.cnpq.br/9876362271623103">  <i class="fa fa-graduation-cap"></i> Lattes</a></div></div>
                                <ul>
                                    <li>Curso de Medicina na Faculdade de Ciências Médicas da Santa Casa de Misericórdia de São Paulo </li> 
                                    <li>Residência em Dermatologia na Santa Casa de Misericódia de São Paulo </li>
                                    <li>Título de Epecialista em Dermatologia pela Sociedade Brasileira de Dermatologia e pela Associação Médica Brasileira </li>
                                    <li>Membro Efetivo da Sociedade Brasileira de Dermatologia </li>
                                    <li>Mestrado em Oncologia Cutânea pelo Hospital de Câncer de Barretos</li>
                                </ul>
                            </div>
                        </div>    
                    </div>

                </div>

                <div class="span6">

                    <div class="corpo-clinico-item">
                        <div class="row-fluid">
                            <div class="span5">
                                <img src="/images/dr-gustavo.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dr. Gustavo Rocha </h3>
                                <div class="especialidade"> Urologista </div>
                                <ul>
                                    <li>Curso de medicina na Faculdade de Medicina de Valença- RJ </li>
                                    <li>Residência Médica em cirurgia geral na Santa Casa de Misericórdia de São Paulo </li>
                                    <li>Residência Médica em Urologia no Complexo Hospitalar Edmundo Vasconcelos em São Paulo </li>
                                    <li>Título de Especialista em Urologia pela Sociedade Brasileira de Urologia e Associação Médica Brasileira </li>
                                    <li>Prêmio Alberto Gentile - Primeiro lugar no concurso para título de especialista pela Sociedade Brasileira de Urologia em 2013 </li>
                                    <li>Membro da Sociedade Brasileira de Urologia </li>
                                    <li>Membro da Sociedade Americana de Urologia</li>
                                </ul>
                            </div>                        
                        </div>
                    </div>

                </div>

            </div>
            <div class="row-fluid">    

                <div class="span6">

                    <div class="corpo-clinico-item">
                        <div class="row-fluid5">
                            <div class="span5">
                                <img src="/images/dr-fransergio.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dr. Fransérgio Cavallari </h3>
                                <div class="especialidade"> Otorrinolaringologista </div>
                                    <ul>
                                        <li>Formação Acadêmica - Faculdade de Ciências Médicas da Unicamp (2000)</li>
                                        <li>Residência Médica em Otorrinolaringologia - HC da Faculdade de Medicina da USP em Ribeirão Preto (2004) </li>
                                        <li>Fellow em Sinusologia, Cirurgia Endoscópica Nasossinusal e Laser em Otorrinolaringologia pelo Bhatt Laser Research Institute e Universidade de Illinois em Chicago - EUA (2004) </li>
                                        <li>Doutorado em Otorrinolaringologia pela Universidade de São Paulo (2012) </li>
                                        <li>Professor Doutor na Faculdade de Ciências da Saúde de Barretos (FACISB), curso de Medicina (2014-hoje)</li>
                                    </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="span6">

                    <div class="corpo-clinico-item">
                        <div class="row-fluid">
                            <div class="span5">
                                <img src="/images/dra-carolina.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dra. Carolina Sasdelli </h3>
                                <div class="especialidade"> Reumatologista </div>
                                Em breve mais informações
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            
            <div class="row-fluid">

                <div class="span6">
                    <div class="corpo-clinico-item">
                        <div class="row-fluid">
                            <div class="span5">
                                <img src="/images/dr-gustavo-sasdeli.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dr. Gustavo Sasdelli </h3>
                                <div class="especialidade"> Neurologista </div>
                                Em breve mais informações
                            </div>
                        </div>
                    </div>

                </div>

                <div class="span6">
                    <div class="corpo-clinico-item">
                        <div class="row-fluid">
                            <div class="span5">
                                <img src="/images/dr-andreza.jpg">
                            </div>
                            <div class="span7">
                                <h3 class="sidebox-title"> Dra. Andreza Vargas </h3>
                                <div class="especialidade"> Endocrinologista </div>
                                Em breve mais informações
                            </div>
                        </div>
                    </div>

                </div>

                
				<!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')