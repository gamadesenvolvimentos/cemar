@include('partials.header')

    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Endocrinologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>endocrinologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        <img src="/images/endrocrinologia.png">
                        A endocrinologia é uma especialidade médica que tem como objetivo diagnosticar e tratar as doenças secundárias a distúrbios hormonais e metabólicos. Trata-se uma área estritamente clínica, não envolvendo procedimentos cirúrgicos. Dentre os órgãos responsáveis pela produção hormonal, que são avaliados pela especialidade, estão: hipófise, tireoide, paratireoide, pâncreas, adrenais, ovários, testículos. A maioria das doenças tratadas são consideradas crônicas, fazendo com que o paciente necessite de acompanhamento de rotina, com especialista, por toda a vida.
                    </p>
                    <p>    
                        Para ser um endocrinologista é preciso 6 anos de graduação em Medicina, 2 anos de especialização em Clínica Médica, 2 anos de especialização em Endocrinologia, complementados por Título de Especialista concedido pela Sociedade Brasileira de Endocrinologia e Metabologia.
                    </p>
                    <p>     
                        O principal desafio da Endocrinologia é conscientizar os pacientes de que não basta seguir as orientações de tratamento medicamentoso mas também é de extrema importância manter hábitos de vida saudáveis (alimentação balanceada, atividade física regular, controle do estresse) para o melhor controle das doenças.
                    </p>
                    <br>
                    <h3><i class="fa fa-question-circle"></i> Perguntas frequentes sobre endocrinologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> Quais os tipos de diabetes? </h3>
                        <p class="collapse" id="p1">
                           O diabetes basicamente pode ser classificado em diabetes tipo 1 (acomete principalmente crianças, adolescentes e adultos jovens, não tem herança familiar  e surge em decorrência de insuficiência total da produção de insulina: o tratamento é feito com insulina); diabetes tipo 2 (acomete principalmente pessoas obesas e sedentárias com mais de 40 anos, tem herança familiar  e surge devido dificuldades de ação da insulina: o tratamento inicial é feito com medicações orais); diabetes gestacional (acomete mulheres grávidas principalmente após a 24&ordf;  semana de gestação com cura após o parto: o tratamento é feito com insulina); diabetes secundário a problemas no pâncreas (pancreatite, tumor de pâncreas) a outras doenças endócrinas (hipertireoidismo, síndrome dos ovários policísticos, excesso de cortisol ou hormônio do crescimento).

                        </p>
                        
                        <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> Quais os fatores de risco para desenvolver diabetes? </h3>
                        <p class="collapse" id="p2">
                          Obesidade, sedentarismo, idade acima de 45 anos, histórico familiar de diabetes (parentes de primeiro grau), diabetes gestacional prévio, síndrome dos ovários policísticos, uso crônico de corticoides.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p3" class="pergunta"> Hipotireoidismo engorda? </h3>
                        <p class="collapse" id="p3">
                          O hipotireoidismo descompensado causa um acúmulo de líquidos e uma redução do metabolismo basal podendo levar a um aumento pequeno de peso enquanto descompensado. Após o tratamento adequado, deve-se voltar ao peso habitual

                        </p>
                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Dra. Andreza Vargas</h3>
                        <img src="/images/dr-andreza.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="">veja mais</a>   
                        </p>
                        <hr>
                        <h3 class="sidebox-title">Doenças tratadas na Endocrinologia</h3>
                        <p> 
                            <ul>
                                <li>Diabetes</li>
                                <li>Hipotireoidismo/Hipertireoidismo</li>
                                <li>Nódulos da tireoide</li>
                                <li>Dislipidemias</li>
                                <li>Doenças das glândulas adrenais</li>
                                <li>Doenças da hipófise</li>
                                <li>Distúrbios de puberdade</li>
                                <li>Distúrbios de crescimento</li>
                                <li>Osteoporose</li>
                                <li>Andropausa</li>
                            </ul>
                        </p>
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')