@include('partials.header')

    <hr>
    <div class="contentArea">

        <div class="divPanel notop page-content">
            <div class="title-especialidade">
                Urologia
            </div>
        
            <div class="breadcrumbs">
                <a href="/">Home</a> &nbsp;/&nbsp; <a href="/especialidades">especialidades</a> &nbsp;/&nbsp; <span>urologia </span>
            </div>

            <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span8" id="divMain">

                    <p>
                        <img src="/images/urologia.png">
                        A Urologia é uma especialidade médica clínico-cirúrgica dedicada ao estudo do aparelho urinário (rins, ureteres, bexiga e uretra) de homens e mulheres e do sistema reprodutor masculino (pênis, próstata, vesículas seminais, ductos deferentes, epidídimos e testículos). Para se tornar um urologista são necessários 6 anos de formação em medicina, 2 anos de residência médica em cirurgia geral e mais 3 anos de residência médica em urologia. Após 11 anos de estudo, os médicos especializados nesta área estão treinados para diagnosticar, prescrever medicamentos, atuar na prevenção de doenças e realizar cirurgias no sistema reprodutor masculino e no aparelho urinário de homens e mulheres. 
                        <br><br>
                        O principal desafio é difundir o conceito que este profissional não trata apenas de doença e sim da qualidade de vida, sendo o urologista uma grande aliado da saúde de homens e mulheres nas diversas etapas da vida.                    
                    </p>
                    <br>
                    <h3><i class="fa fa-question-circle"></i> Perguntas frequentes sobre urologia</h3>
                    <hr>
                    <div class="perguntas-frequentes">
                        
                        <h3 data-toggle="collapse" data-target="#p1" class="pergunta"> Quando deve-se começar a fazer o exame de próstata? </h3>
                        <p class="collapse" id="p1">
                          Todos os homens com mais de 50 anos devem fazer os exames preventivos uma vez ao ano. <br> Entretanto, aqueles com algum familiar com câncer de próstata devem procurar o urologista a partir dos 45 anos de idade.
                        </p>
                        
                        <h3 data-toggle="collapse" data-target="#p2" class="pergunta"> Quais são os exames preventivos do câncer de próstata? </h3>
                        <p class="collapse" id="p2">
                          Utilizamos para rastreamento do cancer de próstata  o PSA (Antígeno Prostático Específico) e o toque retal. Em algumas situações específicas podem serem solicitados outros exames como ultrassom, ressonância magnética e PCA3.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p3" class="pergunta"> Todo homem desenvolverá a Andropausa? </h3>
                        <p class="collapse" id="p3">
                          Não são todos os homens que desenvolveram a andropausa, mas a verdade é que a produção do hormônio testosterona costuma diminuir cerca de 1% ao ano, quando os homens ultrapassam os 40 anos. Isso é fisiológico e natural. Entretanto, quando essa queda é mais acentuada, o fenômeno leva o nome de Distúrbio Androgênico do Envelhecimento Masculino (DAEM) ou popularmente conhecida como andropausa.
                          <br>
                          <br>
                          A doença é caracterizada pela queda do hormônio sexual masculino, a testosterona, acompanhada de sintomas clínicos típicos como diminuição da libido (desejo sexual), do desempenho sexual, cansaço físico e mental, perda de massa muscular, aumento de gordura da região abdominal e em alguns casos, osteoporose . Recomenda­se a qualquer homem com mais de 40 anos e que tenha algum dos sintomas acima descritos a procurar um médico para confirmar o diagnostico.
                          <br>
                          <br>
                          Mudanças de hábitos alimentares, atividades físicas, diminuição de fatores de risco como fumo, álcool, drogas, sedentarismo, excesso de gordura, sal e açúcar podem melhorar as condições gerais do homem. Se necessário, a reposição hormonal pode ser feita através de injeção intramuscular, adesivos, comprimidos por via oral e pelo gel de testosterona com  melhora na condição física, mental e sexual do indivíduo. Lembrando que o tratamento é seguro e eficaz, quando bem indicado.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p4" class="pergunta"> Como é feito a Vasectomia? </h3>
                        <p class="collapse" id="p4">
                          A vasectomia é um procedimento cirúrgico simples realizado no homem para evitar a gravidez. É um método comum de contracepção, considerado seguro e eficaz. O procedimento consiste na interrupção dos canais deferentes, por onde os espermatozóides produzidos nos testículos chegam até a próstata, para serem eliminados em uma ejaculação.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p5" class="pergunta"> Vasectomia causa problemas de ereção? </h3>
                        <p class="collapse" id="p5">
                          Definitivamente não! Não existe relação alguma entre o procedimento e apotência e/ou performance sexual.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p6" class="pergunta"> Vasectomia diminui a ejaculação? </h3>
                        <p class="collapse" id="p6">
                          Praticamente não ocorre mudança na quantidade do líquido ejaculado porque grande parte dele vem das vesículas seminais e não dos ductos deferentes. O homen continua a ejacular, sendo que o líquido seminal apenas não conterá mais espermatozoides;
                        </p>

                        <h3 data-toggle="collapse" data-target="#p7" class="pergunta"> Vasectomia diminui a libido do homem? </h3>
                        <p class="collapse" id="p7">
                          Os hormônios produzidos pelo testículo vão para o corpo via corrente sanguínea, portanto não ocorre qualquer alteração na libido.
                        </p>

                        <h3 data-toggle="collapse" data-target="#p8" class="pergunta"> Como é feito o tratamento do cálculo renal? </h3>
                        <p class="collapse" id="p8">    
                          O tratamento do cálculo renal avançou muito nos últimos anos. Atualmente a grande maioria dos casos podem serem tratados por procedimentos minimamentes invasivos que não utilizam cortes  ou incisões. A melhor abordagem depende de varios fatores como a localização, tamanho, presença de infecção, sintomas e tipo do cálculo. De modo geral dividimos a abordagem em 5 tipos: Tratamento clinico ( com remédios), Litotripsia extracorpórea por ondas de choque ( LECO), cirurgia endoscópica ( sem cortes), cirurgia percutânea ( pequena incisão na região lombar para acessar o rim) e a cirurgia convencional. Se você possui pedra no rim consulte um urologista para saber o melhor tratamento no seu caso.
                        </p>

                    </div>

             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span4 sidebar">

                    <div class="sidebox">

                        <h3 class="sidebox-title">Dr. Gustavo Rocha</h3>
                        <img src="/images/dr-gustavo.jpg" class="img-medico-especialidade">
                        <p>
                            <a href="/corpoclinico">veja mais</a>   
                        </p>
                        <hr>
                        <h3 class="sidebox-title">Doenças tratadas na Urologia</h3>
                        <div class="doencas-list" id="doencas">
                            <p> 
                                <ul>
                                    <li> Cólica renal</li>
                                    <li> Litíase Urinária ( pedra no rim,  na bexiga e nos ureteres)</li>
                                    <li> Incontinência urinária ( perda de urina) masculina e feminina </li>
                                    <li> Ejaculação  precoce (rápida) e ejaculação retardada</li>
                                    <li> Disfunção erétil (impotência sexual)</li>
                                    <li> Fimose</li>
                                    <li> Freio balanoprepucial curto</li>
                                    <li> Vasectomia e reversão</li>
                                    <li> Candidíase peniana</li>
                                    <li> Doença sexualmente transmitida ( sífilis, cancro duro, cancro mole, uretrite, gonorreia, etc... )</li>
                                    <li> HPV (condiloma peniano, crista de galo)</li>
                                    <li> Doença de Peyronie (curvatura peniana)</li>
                                    <li> Orquialgia (dor testicular)</li>
                                    <li> Hemospermia (sangue no esperma)</li>
                                    <li> Hematúria ( sangue na urina)</li>
                                    <li> Disfunções miccionais (dificuldade de urinar ou segurar a urina)</li>
                                    <li> Hiperplasia Prostática Benigna ( HPB - crescimento benigno da próstata)</li>
                                    <li> Prostatite</li>
                                    <li> Pielonefrite</li>
                                    <li> Cistite</li>
                                    <li> Orquite</li>
                                    <li> Epididimite</li>
                                    <li> Cistos Renais</li>
                                    <li> Balanite Xerótica Obliterante</li>
                                    <li> Balanopostite</li>
                                    <li> Bexiga Urinaria Neurogênica</li>
                                    <li> Bexiga Urinária Hiperativa</li>
                                    <li> Estenose de uretra</li>
                                    <li> Estenose de ureter</li>
                                    <li> Estenose de JUP ( junção ureteropiélica)</li>
                                    <li> Prevenção de câncer de próstata </li>
                                    <li> Câncer de prostata</li>
                                    <li> Câncer de bexiga</li>
                                    <li> Câncer de rim</li>
                                    <li> Câncer de adrenal</li>
                                    <li> Câncer de penis</li>
                                    <li> Câncer de testiculos</li>
                                </ul>
                            </p>
                        </div>
                        <div class="doencas-more" onclick="showdoencas();" id="showdoencas">
                            mostrar mais doenças<br>
                            <i class="fa fa-caret-down"></i>
                        </div>
                        <div class="doencas-less" onclick="hidedoencas();" id="hidedoencas">
                            <i class="fa fa-caret-up"></i><br>
                            mostrar menos doenças
                        </div>

                        <a href="/urologia-calculo-renal-mitos-e-vedades">                        
                            <div class="bot-procedimentos">
                                <div class="row-fluid" style="padding-top:7px;">
                                    <div class="span3 icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="span9">
                                        <div class="desc">
                                            Cálculo renal: mitos e verdades
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>  

                        <a href="/urologia-estudo-urodinamico">                        
                            <div class="bot-procedimentos">
                                <div class="row-fluid" style="padding-top:7px;">
                                    <div class="span3 icon">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="span9">
                                        <div class="desc">
                                            Estudo Urodinâmico
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>    
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>

<script>

    function showdoencas() {
        
        document.getElementById("doencas").style.height = "100%";
        document.getElementById("showdoencas").style.display = "none";
        document.getElementById("hidedoencas").style.display = "block";

    } 

    function hidedoencas() {
        document.getElementById("hidedoencas").style.display = "none";
        document.getElementById("showdoencas").style.display = "block";
        document.getElementById("doencas").style.height = "300px";
    }

</script>
@include('partials.footer')