@include('partials.header')

    <hr>
    <div class="contentArea">

    <div class="head-sugestoes">
        
    </div>

        <div class="divPanel notop page-content">

            <div class="breadcrumbs">
                <a href="index.html">Home</a> &nbsp;/&nbsp; <span>Contato</span>
            </div>

            <div class="row-fluid">
                <div class="span8" id="divMain">

                    <h1>Entre em contato conosco</h1>
                    {!! csrf_field() !!}
                          
                      <div class="success" {!! (session('success')) ? 'style="display:block;"' : '' !!}> 
                        E-mail enviado com sucesso! 
                        <strong> Em breve nós entraremos em contato com você! =D.</strong> 
                      </div>

                    <hr>
                    <!--Start Contact form -->                                                      
                    <form name="enq" id="contact-form" method="post" action="/contato" >
                      <fieldset>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="text" name="name" id="name" value=""  class="input-block-level" placeholder="Digite seu nome" />
                        <input type="text" name="email" id="email" value="" class="input-block-level" placeholder="Digite seu e-mail" />
                        <textarea rows="5" name="mensagem" id="mensagem" class="input-block-level" placeholder="Comentario"></textarea>
                        <div class="actions">
                        <input type="submit" value="Enviar" name="submit" id="submitButton" class="btn btn-info pull-right" title="Clique aqui para enviar seu contato" onclick="javascript:document.getElementById('contact-form').submit();" />
                        </div>
                        
                        </fieldset>
                    </form>                  
                    <!--End Contact form -->

                </div>
                
                <!--Edit Sidebar Content here-->    
                <div class="span4 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Informações para contato</h3>
                    <p>
                        <strong> Telefones: </strong> <br>
                        (17) 3322 6108 <br> (17) 99259 0742<br>
                        <hr>
                        <strong> Endereço: </strong> <br>
                        Rua 26, 951, Centro, Barretos-SP  
                    </p>     
                     
                     <!-- Start Side Categories -->
        
                    <!-- End Side Categories -->
                                        
                    </div>
       
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <h1>Como chegar</h1>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.7426791476555!2d-48.57522748538122!3d-20.557706763442425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94bb853b4e0ce8fb%3A0xe28cf03314aa3b05!2sR.+Vinte+e+Seis%2C+951+-+Santana%2C+Barretos+-+SP%2C+14781-373!5e0!3m2!1spt-BR!2sbr!4v1447851225846" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>    
                    </div>
                </div>
            <!--/End Sidebar Content-->
                
                
            </div>

            <div id="footerInnerSeparator"></div>
        </div>
    </div>


@include('partials.footer')